<?php

use App\Http\Controllers\CustomerController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\PurchaseController;
use App\Http\Controllers\PurchaseReturnController;
use App\Http\Controllers\ReportController;
use App\Http\Controllers\SellController;
use App\Http\Controllers\SellReturnController;
use App\Http\Controllers\SiteController;
use App\Http\Controllers\SupplierController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/',[SiteController::class,'index'])->name('login');
Route::post('/login',[SiteController::class,'login'])->name('storeLogin');



Route::prefix('admin')->name('admin.')->middleware(['auth'])->group(function(){
    Route::get('/',[DashboardController::class,'index'])->name('dashboard');

    Route::get('/logout',[SiteController::class,'logout'])->name('logout');

    //Product Routes
    Route::prefix('product')->name('products.')->group(function(){
        Route::get('/',[ProductController::class,'index'])->name('index');
        Route::get('/create',[ProductController::class,'create'])->name('create');
        Route::post('/store',[ProductController::class,'store'])->name('store');
        Route::get('/delete/{id}',[ProductController::class,'destroy'])->name('destroy');
        Route::get('/edit/{id}',[ProductController::class,'edit'])->name('edit');
        Route::post('/update/{id}',[ProductController::class,'update'])->name('update');
        Route::get('/getDataById',[ProductController::class,'getDataById'])->name('getDataById');
    });


    //Customer Routes
    Route::prefix('customer')->name('customer.')->group(function(){
        Route::get('/',[CustomerController::class,'index'])->name('index');
        Route::get('/create',[CustomerController::class,'create'])->name('create');
        Route::post('/store',[CustomerController::class,'store'])->name('store');
        Route::get('/delete/{id}',[CustomerController::class,'destroy'])->name('destroy');
        Route::get('/edit/{id}',[CustomerController::class,'edit'])->name('edit');
        Route::post('/update/{id}',[CustomerController::class,'update'])->name('update');
    });

    //Supplier Routes
    Route::prefix('supplier')->name('supplier.')->group(function(){
        Route::get('/',[SupplierController::class,'index'])->name('index');
        Route::get('/create',[SupplierController::class,'create'])->name('create');
        Route::post('/store',[SupplierController::class,'store'])->name('store');
        Route::get('/delete/{id}',[SupplierController::class,'destroy'])->name('destroy');
        Route::get('/edit/{id}',[SupplierController::class,'edit'])->name('edit');
        Route::post('/update/{id}',[SupplierController::class,'update'])->name('update');
    });

    //Sell Routes
    Route::prefix('sell')->name('sell.')->group(function(){
        Route::get('/',[SellController::class,'index'])->name('index');
        Route::get('/create',[SellController::class,'create'])->name('create');
        Route::post('/store',[SellController::class,'store'])->name('store');
        Route::get('/delete/{id}',[SellController::class,'destroy'])->name('destroy');
        Route::get('/detail/{id}',[SellController::class,'show'])->name('show');
    });

    //Purchase Routes
    Route::prefix('purchase')->name('purchase.')->group(function(){
        Route::get('/',[PurchaseController::class,'index'])->name('index');
        Route::get('/create',[PurchaseController::class,'create'])->name('create');
        Route::post('/store',[PurchaseController::class,'store'])->name('store');
        Route::get('/delete/{id}',[PurchaseController::class,'destroy'])->name('destroy');
        Route::get('/detail/{id}',[PurchaseController::class,'show'])->name('show');
    });

    //Sell Return Routes
    Route::prefix('sell_return')->name('sell_return.')->group(function(){
        Route::get('/',[SellReturnController::class,'index'])->name('index');
        Route::get('/create',[SellReturnController::class,'create'])->name('create');
        Route::post('/store',[SellReturnController::class,'store'])->name('store');
        Route::get('/delete/{id}',[SellReturnController::class,'destroy'])->name('destroy');
        Route::get('/detail/{id}',[SellReturnController::class,'show'])->name('show');
    });

    //Purchase Return Routes
    Route::prefix('purchase_return')->name('purchase_return.')->group(function(){
        Route::get('/',[PurchaseReturnController::class,'index'])->name('index');
        Route::get('/create',[PurchaseReturnController::class,'create'])->name('create');
        Route::post('/store',[PurchaseReturnController::class,'store'])->name('store');
        Route::get('/delete/{id}',[PurchaseReturnController::class,'destroy'])->name('destroy');
        Route::get('/detail/{id}',[PurchaseReturnController::class,'show'])->name('show');
    });
    //Report Routes
    Route::prefix('report')->name('report.')->group(function(){
        Route::get('/',[ReportController::class,'report'])->name('report');
    });

});




