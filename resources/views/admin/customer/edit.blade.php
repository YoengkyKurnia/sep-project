@extends('partials.main')
@section('content')
@include('partials.sidebar')
<div class="content-page">
     <div class="container-fluid add-form-list">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header d-flex justify-content-between">
                        <div class="header-title">
                            <h4 class="card-title">Edit Customer</h4>
                        </div>
                    </div>
                    <div class="card-body">
                        <form action="{{route('admin.customer.update',$customer->id)}}" method="POST" enctype="multipart/form-data" data-toggle="validator">
                            @csrf
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Name *</label>
                                        <input type="text" class="form-control" name="name" value="{{$customer->name}}" placeholder="Enter Name" data-errors="Please Enter Name." required>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Email *</label>
                                        <input type="text" class="form-control" name="email" value="{{$customer->email}}" placeholder="Enter Email" data-errors="Please Enter Email." required>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Phone *</label>
                                        <input type="number" class="form-control" name="phone" value="{{$customer->phone}}" placeholder="Enter Phone" required>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Address</label>
                                        <textarea class="form-control" rows="4" name="address">{{$customer->address}}</textarea>
                                    </div>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary mr-2">Update Customer</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- Page end  -->
@endsection
