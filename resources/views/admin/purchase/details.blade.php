@extends('partials.main')
@section('content')
@include('partials.sidebar')
<div class="content-page">
      <div class="container-fluid">
         <div class="row">
            <div class="col-lg-12">
               <div class="card card-block card-stretch card-height print rounded">
                  <div class="card-header d-flex justify-content-between bg-primary header-invoice">
                        <div class="iq-header-title">
                           <h4 class="card-title mb-0">{{$transaction->invoice_number}}</h4>
                        </div>
                  </div>
                  <div class="card-body">
                        <div class="row">
                           <div class="col-lg-12">
                              <div class="table-responsive-sm">
                                    <table class="table">
                                       <thead>
                                          <tr>
                                                <th scope="col">Order Date</th>
                                                <th scope="col">Supplier Name</th>
                                                <th scope="col">Supplier Email</th>
                                                <th scope="col">Phone Number</th>
                                                <th scope="col">Address</th>
                                          </tr>
                                       </thead>
                                       <tbody>
                                          <tr>
                                                <td>{{date('d M Y H:i',strtotime($transaction->created_at))}}</td>
                                                <td>{{$transaction->customer->name}}</td>
                                                <td>{{$transaction->customer->email}}</td>
                                                <td>{{$transaction->customer->phone}}</td>
                                                <td>{{$transaction->customer->address}}</td>
                                          </tr>
                                       </tbody>
                                    </table>
                              </div>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-sm-12">
                              <h5 class="mb-3">Purchase Summary</h5>
                              <div class="table-responsive-sm">
                                    <table class="table">
                                       <thead>
                                          <tr>
                                                <th class="text-center" scope="col">#</th>
                                                <th scope="col">Item</th>
                                                <th class="text-center" scope="col">Quantity</th>
                                                <th class="text-center" scope="col">Price</th>
                                                <th class="text-center" scope="col">Totals</th>
                                          </tr>
                                       </thead>
                                       <tbody>
                                           @foreach ($transaction->transactionDetails as $key => $value)
                                                <tr>
                                                        <th class="text-center" scope="row">{{$key+1}}</th>
                                                        <td>
                                                            <h6 class="mb-0">{{$value->product->name}}</h6>
                                                        </td>
                                                        <td class="text-center">{{$value->qty}}</td>
                                                        <td class="text-center">Rp. {{number_format($value->sell_price,2,',','.')}}</td>
                                                        <td class="text-center"><b>Rp. {{number_format($value->sell_price * $value->qty,2,',','.')}}</b></td>
                                                </tr>
                                           @endforeach
                                       </tbody>
                                    </table>
                              </div>
                           </div>
                        </div>
                        <div class="row mt-4 mb-3">
                           <div class="offset-lg-8 col-lg-4">
                              <div class="or-detail rounded">
                                    <div class="p-3">
                                       <h5 class="mb-3">Purchase Details</h5>
                                       <div class="mb-2">
                                          <h6>Sub Total</h6>
                                          <p>Rp. {{number_format($transaction->total,2,',','.')}}</p>
                                       </div>
                                       <div class="mb-2">
                                          <h6>Discount</h6>
                                          <p>Rp. {{number_format($transaction->discount,2,',','.')}}</p>
                                       </div>
                                       <div class="mb-2">
                                          <h6>Tax (%)</h6>
                                          <p>{{number_format($transaction->tax,1,',','.')}} %</p>
                                       </div>
                                    </div>
                                    <div class="ttl-amt py-2 px-3 d-flex justify-content-between align-items-center">
                                       <h6>Total</h6>
                                       <h3 class="text-primary font-weight-700">Rp. {{number_format($transaction->grand_final,2,',','.')}}</h3>
                                    </div>
                              </div>
                           </div>
                        </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      </div>
    </div>
@endsection
