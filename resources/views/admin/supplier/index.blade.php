@extends('partials.main')
@section('content')
@include('partials.sidebar')
<div class="content-page">
    <div class="container-fluid">
       <div class="row">
           <div class="col-lg-12">
               <div class="d-flex flex-wrap flex-wrap align-items-center justify-content-between mb-4">
                   <div>
                       <h4 class="mb-3">Supplier List</h4>
                   </div>
                   <a href="{{route('admin.supplier.create')}}" class="btn btn-primary add-list"><i class="las la-plus mr-3"></i>Add Supplier</a>
               </div>
           </div>
           <div class="col-lg-12">
               <div class="table-responsive rounded mb-3">
               <table class="data-tables table mb-0 tbl-server-info">
                   <thead class="bg-white text-uppercase">
                       <tr class="ligth ligth-data">
                           <th>Name</th>
                           <th>Email</th>
                           <th>Phone Number</th>
                           <th>Address</th>
                           <th>Action</th>
                       </tr>
                   </thead>
                   <tbody class="ligth-body">
                       @foreach ($suppliers as $supplier)
                            <tr>
                                <td>{{$supplier->name}}</td>
                                <td>{{$supplier->email}}</td>
                                <td>{{$supplier->phone}}</td>
                                <td>{{$supplier->address}}</td>
                                <td>
                                    <div class="d-flex align-items-center list-action">
                                        <a class="badge bg-success mr-2" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit"
                                            href="{{route('admin.supplier.edit',$supplier->id)}}"><i class="ri-pencil-line mr-0"></i></a>
                                        <a class="badge bg-warning mr-2" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete"
                                            href="{{route('admin.supplier.destroy',$supplier->id)}}" onclick="return confirm('Are you Sure?')"><i class="ri-delete-bin-line mr-0"></i></a>
                                    </div>
                                </td>
                            </tr>
                       @endforeach
                   </tbody>
               </table>
               </div>
           </div>
       </div>
       <!-- Page end  -->
   </div>
@endsection
