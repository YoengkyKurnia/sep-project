@extends('partials.main')
@section('content')
@include('partials.sidebar')
<div class="content-page">
    <div class="container-fluid">
       <div class="row">
           <div class="col-lg-12">
               <div class="d-flex flex-wrap flex-wrap align-items-center justify-content-between mb-4">
                   <div>
                       <h4 class="mb-3">Sell List</h4>
                   </div>
                   <a href="{{route('admin.sell.create')}}" class="btn btn-primary add-list"><i class="las la-plus mr-3"></i>Add Sell</a>
               </div>
           </div>
           <div class="col-lg-12">
               <div class="table-responsive rounded mb-3">
               <table class="data-tables table mb-0 tbl-server-info">
                   <thead class="bg-white text-uppercase">
                       <tr class="ligth ligth-data">
                        <th>Transaction Date</th>
                           <th>Invoice Number</th>
                           <th>Total Sell</th>
                           <th>Discount</th>
                           <th>Tax</th>
                           <th>Final Total</th>
                           <th>Action</th>
                       </tr>
                   </thead>
                   <tbody class="ligth-body">
                       @foreach ($sells as $sell)
                            <tr>
                                <td>{{date('d M Y H:i',strtotime($sell->created_at))}}</td>
                                <td>{{$sell->invoice_number}}</td>
                                <td>Rp. {{number_format($sell->total,2,',','.')}}</td>
                                <td>Rp. {{number_format($sell->discount,2,',','.')}}</td>
                                <td>{{number_format($sell->tax,1,',','.')}} %</td>
                                <td>Rp. {{number_format($sell->grand_final,2,',','.')}}</td>
                                <td>
                                    <div class="d-flex align-items-center list-action">
                                        <a class="badge bg-success mr-2" data-toggle="tooltip" data-placement="top" title="" data-original-title="Details"
                                            href="{{route('admin.sell.show',$sell->id)}}">
                                            <svg class="svg-icon" id="p-dash7" width="20" height="20" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
                                                <path d="M14 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V8z"></path><polyline points="14 2 14 8 20 8"></polyline><line x1="16" y1="13" x2="8" y2="13"></line><line x1="16" y1="17" x2="8" y2="17"></line><polyline points="10 9 9 9 8 9"></polyline>
                                            </svg>
                                        </a>
                                        <a class="badge bg-warning mr-2" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete"
                                            href="{{route('admin.sell.destroy',$sell->id)}}" onclick="return confirm('Are you Sure?')"><i class="ri-delete-bin-line mr-0"></i></a>
                                    </div>
                                </td>
                            </tr>
                       @endforeach
                   </tbody>
               </table>
               </div>
           </div>
       </div>
       <!-- Page end  -->
   </div>
@endsection
