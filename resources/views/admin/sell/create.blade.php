@extends('partials.main')
@section('content')
@include('partials.sidebar')
<div class="content-page">
     <div class="container-fluid add-form-list">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header d-flex justify-content-between">
                        <div class="header-title">
                            <h4 class="card-title">Add Sell</h4>
                        </div>
                    </div>
                    <div class="card-body">
                        <form action="{{route('admin.sell.store')}}" method="POST" enctype="multipart/form-data" data-toggle="validator">
                            @csrf
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Invoice Number *</label>
                                        <input type="text" class="form-control" name="invoice_number" placeholder="Enter Invoice Number (Blank for auto Generate)">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Customer *</label>
                                        <select name="customer" class="form-control select2">
                                            @foreach ($customers as $customer)
                                                <option value="{{$customer->id}}">{{$customer->name}}</option>
                                            @endforeach
                                        </select>
                                         <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Product</label>
                                        <select name="product" id="product" class="form-control select2">
                                            <option value="" readonly>Select Product</option>
                                            @foreach ($products as $product)
                                                <option value="{{$product->id}}">{{$product->name}}</option>
                                            @endforeach
                                        </select>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="table-responsive rounded mb-3">
                                    <table class="data-tables-no-search table mb-0 tbl-server-info">
                                        <thead class="bg-white text-uppercase">
                                            <tr class="ligth ligth-data">
                                                <th>Product</th>
                                                <th>Price</th>
                                                <th>Qty</th>
                                                <th>Subtotal</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody class="ligth-body" id="product_data">

                                        </tbody>
                                    </table>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Tax</label>
                                        <input type="number" class="form-control" name="tax" placeholder="Enter Tax in Percent (%)" data-errors="Enter Tax in Percent(%)">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Discount </label>
                                        <input type="number" class="form-control" name="discount" placeholder="Enter Discount (Fixed Rate)" data-errors="Enter Discount (Fixed Rate).">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary mr-2">Add Sell</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- Page end  -->
@endsection
@section('script')
    <script>
        function numberWithCommas(x) {
            return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        }
        $(document).ready(function() {
            $('.select2').select2({
                theme: "bootstrap4"
            });
        });
        $('.select2').on('select2:select', function (e) {
            var data = e.params.data.id;
            if(data != ""){
                $.ajax({
                    url :"{{route('admin.products.getDataById')}}",
                    data : {id:data},
                    cache:false,
                    success:function(data){
                        var stringAppend = "";
                        stringAppend += "<tr>";
                        stringAppend += "<input type='hidden' name='id[]' value='"+data.id+"'>";
                        stringAppend += "<td>"+data.name+"</td>";
                        stringAppend += "<td data-price='"+data.sell_price+"' class='sell_price'>Rp. "+numberWithCommas(data.sell_price)+"</td>";
                        stringAppend += "<td class='col-md-2'><input type='text' class='form-control changeSubtotal' value='1' min='1' name='qty[]'></td>";
                        stringAppend += "<td class='subtotal'>Rp. "+numberWithCommas(data.sell_price)+"</td>";
                        stringAppend += '<td><div class="d-flex align-items-center list-action"> <a class="badge bg-warning mr-2 deleteLine" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete"><i class="ri-delete-bin-line mr-0"></i></a></div></td>';
                        stringAppend += "</tr>";
                        $("#product_data").append(stringAppend);
                    }
                });
            }
        });

        $(document).on("click",".deleteLine",function(){
            $(this).parent().parent().parent().remove();
        });
        $(document).on("keyup",".changeSubtotal",function () {
            if (this.value != this.value.replace(/[^0-9\.]/g, '')) {
                this.value = this.value.replace(/[^0-9\.]/g, '');
            }
            var price = $(this).parent().parent().find('.sell_price').data("price");
            var qty = $(this).val();
            var total = qty * price;
            $(this).parent().parent().find(".subtotal").text("Rp. "+numberWithCommas(total));
        });
    </script>
@endsection
