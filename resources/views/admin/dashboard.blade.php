@extends('partials.main')
@section('content')
    <div class="wrapper">
    @include('partials.sidebar')
    <div class="content-page">
     <div class="container-fluid">
        <div class="row">
            <div class="col-lg-4">
                <div class="card card-transparent card-block card-stretch card-height border-none">
                    <div class="card-body p-0 mt-lg-2 mt-0">
                        <h3 class="mb-3">Hi {{\Auth::user()->name}}, Good Morning</h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
      </div>
    </div>
    @include('partials.footer')
@endsection
