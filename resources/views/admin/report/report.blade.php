@extends('partials.main')
@section('content')
@include('partials.sidebar')
<div class="content-page">
    <div class="container-fluid">
       <div class="row">
           <div class="col-lg-12">
               <div class="d-flex flex-wrap flex-wrap align-items-center justify-content-between mb-4">
                   <div>
                       <h4 class="mb-3">{{\App\Models\TransactionDetails::tipe($type)}} List</h4>
                   </div>
               </div>
           </div>
           <div class="col-lg-12">
               <div class="table-responsive rounded mb-3">
               <table class="data-tables table mb-0 tbl-server-info">
                   <thead class="bg-white text-uppercase">
                       <tr class="ligth ligth-data">
                           <th>Product</th>
                           <th>Code</th>
                           <th>Quantity</th>
                           <th>Purchase Price</th>
                           @if($type != 'purchase' && $type != 'purchase_return')
                            <th>Sell Price</th>
                           @endif
                           <th>Total</th>
                       </tr>
                   </thead>
                   <tbody class="ligth-body">
                       @foreach ($data as $product)
                            <tr>
                                <td>
                                    <div class="d-flex align-items-center">
                                        <img src="{{$product->image}}" class="img-fluid rounded avatar-50 mr-3" alt="image">
                                        <div>
                                            {{$product->name}}
                                        </div>
                                    </div>
                                </td>
                                <td>{{$product->code}}</td>
                                <td>{{number_format($product->qty_total,2,',','.')}}</td>
                                <td>Rp. {{number_format($product->purchase_price,2,',','.')}}</td>
                                @if($type != 'purchase' && $type != 'purchase_return')
                                    <td>Rp. {{number_format($product->sell_price,2,',','.')}}</td>
                                    <td>Rp. {{number_format(($product->sell_price - $product->purchase_price)*$product->qty_total,2,',','.')}}</td>
                                @else
                                    <td>Rp. {{number_format(($product->purchase_price)*$product->qty_total,2,',','.')}}</td>
                                @endif
                            </tr>
                       @endforeach
                   </tbody>
               </table>
               </div>
           </div>
       </div>
       <!-- Page end  -->
   </div>
@endsection
