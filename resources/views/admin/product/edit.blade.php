@extends('partials.main')
@section('content')
@include('partials.sidebar')
<div class="content-page">
     <div class="container-fluid add-form-list">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header d-flex justify-content-between">
                        <div class="header-title">
                            <h4 class="card-title">Edit Product</h4>
                        </div>
                    </div>
                    <div class="card-body">
                        <form action="{{route('admin.products.update',$product->id)}}" method="POST" enctype="multipart/form-data" data-toggle="validator">
                            @csrf
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Name *</label>
                                        <input type="text" class="form-control" name="name" value="{{$product->name}}" placeholder="Enter Name" data-errors="Please Enter Name." required>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Code *</label>
                                        <input type="text" class="form-control" name="code"  value="{{$product->code}}" placeholder="Enter Code" data-errors="Please Enter Code." required>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Purchase Price *</label>
                                        <input type="number" class="form-control" name="purchase_price"  value="{{$product->purchase_price}}" placeholder="Enter Cost" data-errors="Enter Purchase Price." required>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Selling Price *</label>
                                        <input type="number" class="form-control" name="selling_price"  value="{{$product->sell_price}}" placeholder="Enter Price" data-errors="Enter Selling Price." required>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Quantity *</label>
                                        <input type="number" class="form-control" name="qty" value="{{$product->qty}}" placeholder="Enter Quantity" disabled>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Image</label>
                                        <input type="file" class="form-control image-file" name="image" accept="image/*">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Description / Product Details</label>
                                        <textarea class="form-control" rows="4" name="description">{{$product->description}}</textarea>
                                    </div>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary mr-2">Update Product</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- Page end  -->
@endsection
