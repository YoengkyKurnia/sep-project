@extends('partials.main')
@section('content')
@include('partials.sidebar')
<div class="content-page">
    <div class="container-fluid">
       <div class="row">
           <div class="col-lg-12">
               <div class="d-flex flex-wrap flex-wrap align-items-center justify-content-between mb-4">
                   <div>
                       <h4 class="mb-3">Product List</h4>
                   </div>
                   <a href="{{route('admin.products.create')}}" class="btn btn-primary add-list"><i class="las la-plus mr-3"></i>Add Product</a>
               </div>
           </div>
           <div class="col-lg-12">
               <div class="table-responsive rounded mb-3">
               <table class="data-tables table mb-0 tbl-server-info">
                   <thead class="bg-white text-uppercase">
                       <tr class="ligth ligth-data">
                           <th>Product</th>
                           <th>Code</th>
                           <th>Category</th>
                           <th>Quantity</th>
                           <th>Price</th>
                           <th>Cost</th>
                           <th>Action</th>
                       </tr>
                   </thead>
                   <tbody class="ligth-body">
                       @foreach ($products as $product)
                            <tr>
                                <td>
                                    <div class="d-flex align-items-center">
                                        <img src="{{$product->image}}" class="img-fluid rounded avatar-50 mr-3" alt="image">
                                        <div>
                                            {{$product->name}}
                                        </div>
                                    </div>
                                </td>
                                <td>{{$product->code}}</td>
                                <td>Beauty</td>
                                <td>{{number_format($product->qty,2,',','.')}}</td>
                                <td>Rp. {{number_format($product->sell_price,2,',','.')}}</td>
                                <td>Rp. {{number_format($product->purchase_price,2,',','.')}}</td>
                                <td>
                                    <div class="d-flex align-items-center list-action">
                                        <a class="badge bg-success mr-2" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit"
                                            href="{{route('admin.products.edit',$product->id)}}"><i class="ri-pencil-line mr-0"></i></a>
                                        <a class="badge bg-warning mr-2" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete"
                                            href="{{route('admin.products.destroy',$product->id)}}" onclick="return confirm('Are you Sure?')"><i class="ri-delete-bin-line mr-0"></i></a>
                                    </div>
                                </td>
                            </tr>
                       @endforeach
                   </tbody>
               </table>
               </div>
           </div>
       </div>
       <!-- Page end  -->
   </div>
@endsection
