<?php

namespace App\Http\Controllers;

use App\Models\TransactionDetails;
use Illuminate\Http\Request;

class ReportController extends Controller
{
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            if(\Auth::user()->type != 'admin'){
                return redirect()->back();
            }
            \Session::put('menu','report');
            return $next($request);
        });
    }

    public function report(Request $request)
    {
        $data = TransactionDetails::leftJoin('transactions','transactions.id','=','transactions_details.transaction_id')
                ->leftJoin('products','products.id','=','transactions_details.product_id')
                ->where('transactions.type',$request->type)
                ->select('products.name','products.code','products.image','transactions_details.qty','transactions_details.purchase_price','transactions_details.sell_price',\DB::raw('(SUM(transactions_details.qty)) as qty_total'))
                ->groupBy('transactions_details.product_id')
                ->get();

        $type = $request->type;

        return view('admin.report.report',compact('data','type'));
    }
}
