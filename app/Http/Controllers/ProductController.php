<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ProductController extends Controller
{

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            \Session::put('menu','product');
            return $next($request);
        });
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::all();
        return view('admin.product.index',compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.product.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'name'=>'required',
            'code'=>'required|unique:products,code',
            'qty'=>'required|numeric|min:0',
            'purchase_price'=>'required|numeric|min:0',
            'selling_price'=>'required|numeric|min:0',
        ];

        $data = $request->all();

        $validate = Validator::make($data,$rules);
        if($validate->fails()){
            return redirect()->back()->withErrors('Validation Errors');
        }

        $product = new Product();
        $product->name = $data['name'];
        $product->code = $data['code'];
        $product->qty = $data['qty'];
        $product->sell_price = $data['selling_price'];
        $product->purchase_price = $data['purchase_price'];
        $product->description = $data['description'];
        if($request->file('image')){
            $file = $request->file('image');
            $filename= date('YmdHis').$file->getClientOriginalName();
            $file->move(public_path('image'), $filename);
            $product->image = "/image/".$filename;
        }
        $product->save();

        return redirect()->route('admin.products.index')->with('success','Berhasil Menambahkan Data!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = Product::find($id);
        return view('admin.product.edit',compact('product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = [
            'name'=>'required',
            'code'=>'required|unique:products,code,'.$id,
            'purchase_price'=>'required|numeric|min:0',
            'selling_price'=>'required|numeric|min:0',
        ];

        $data = $request->all();

        $validate = Validator::make($data,$rules);
        if($validate->fails()){
            return redirect()->back()->withErrors('Validation Errors');
            dd($validate->errors());
        }

        $product = Product::find($id);
        $product->name = $data['name'];
        $product->code = $data['code'];
        // $product->qty = $data['qty'];
        $product->sell_price = $data['selling_price'];
        $product->purchase_price = $data['purchase_price'];
        $product->description = $data['description'];
        if($request->file('image')){
            $file = $request->file('image');
            $filename= date('YmdHis').$file->getClientOriginalName();
            $file->move(public_path('image'), $filename);
            $product->image = "/image/".$filename;
        }
        $product->update();

        return redirect()->route('admin.products.index')->with('success','Berhasil Mengubah Data!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::find($id);
        if(!isset($product)){
            return redirect()->back()->withErrors('ID Not Found');
        }
        $product->delete();
        return redirect()->back()->with('success','Berhasil Menghapus Data!');
    }

    public function getDataById(Request $request)
    {

        if(!isset($request->id)){
            return false;
        }
        $product = Product::find($request->id);


        return $product;
    }
}
