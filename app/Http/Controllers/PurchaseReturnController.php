<?php

namespace App\Http\Controllers;

use App\Models\Contact;
use App\Models\Product;
use App\Models\Transaction;
use App\Models\TransactionDetails;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use DB;


class PurchaseReturnController extends Controller
{
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            \Session::put('menu','purchase_return');
            return $next($request);
        });
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $purchases = Transaction::where('type',Transaction::PURCHASE_RETURN)->get();
        return view('admin.purchaseReturn.index',compact('purchases'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $customers = Contact::where('type',Contact::SUPPLIER)->get();
        $products = Product::all();
        return view('admin.purchaseReturn.create',compact('customers','products'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->except(['token','product']);
        DB::beginTransaction();
        try {

            //Save Transaction Data
            $transaction = new Transaction();
            //Invoice Number Save Dummy
            $transaction->invoice_number = '-';
            $transaction->type = Transaction::PURCHASE_RETURN;
            $transaction->customer_id = $data['customer'];
            $transaction->tax = isset($data['tax'])?$data['tax']:0;
            $transaction->discount = isset($data['discount'])?$data['discount']:0;
            $transaction->save();


            $total = 0;

            //Save Transaction Detail Data
            foreach ($data['id'] as $key => $value) {
                $transactionDetail = new TransactionDetails();
                $transactionDetail->transaction_id = $transaction->id;
                $transactionDetail->product_id = $value;
                $transactionDetail->qty = $data['qty'][$key];

                $product = Product::getPricingFromId($value);
                if(!isset($product)){
                    DB::rollback();
                    return redirect()->back()->withErrors('ID Product Not Found !');
                }

                $qty = Product::minusQtyProductFromId($value,$data['qty'][$key]);
                if($qty['success'] == 0){
                    DB::rollback();
                    return redirect()->back()->withErrors($qty['msg']);
                }


                $transactionDetail->purchase_price = $product['purchase_price'];
                $transactionDetail->sell_price = $product['sell_price'];
                $transactionDetail->save();

                $total += $transactionDetail->qty * $transactionDetail->purchase_price;
            }

            $transaction->total = $total;
            $totalWithoutTax = $total - $transaction->discount;
            //Invoice Number for Generated
            $transaction->invoice_number = isset($data['invoice_number'])?$data['invoice_number']:Transaction::generateInvoiceNumber($transaction->id);
            $transaction->grand_final = $totalWithoutTax - (($transaction->tax > 0)?$totalWithoutTax*$transaction->tax/100:0);
            $transaction->save();


            DB::commit();
            return redirect()->route('admin.purchase_return.index')->with('success','Berhasil Menambahkan Data!');
        } catch (\Exception $e) {
            DB::rollback();
            Log::emergency($e->getFile() . ' - ' . $e->getLine() . ' - '. $e->getMessage());
            return redirect()->back()->withErrors($e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $transaction = Transaction::find($id);
        return view('admin.purchaseReturn.details',compact('transaction'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        try {

            $detail = DB::table('transactions_details')->where('id',$id)->delete();
            $transaction = Transaction::where('id',$id)->delete();

            DB::commit();
            return redirect()->route('admin.purchase_return.index')->with('success','Berhasil Menghapus Data!');
        } catch (\Exception $e) {
            DB::rollback();
            Log::emergency($e->getFile() . ' - ' . $e->getLine() . ' - '. $e->getMessage());
            return redirect()->back()->withErrors($e->getMessage());
        }
    }
}
