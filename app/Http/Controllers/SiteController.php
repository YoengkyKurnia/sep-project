<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class SiteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('index');
    }

    public function login(Request $request)
    {
        $rules = [
            'email' => 'required',
            'password' => 'required'
        ];

        $data = $request->all();
        $validate = Validator::make($data,$rules);

        if($validate->fails()){
            return redirect()->back();
        }

        if(\Auth::attempt(['email' => $data['email'], 'password' => $data['password']])){
            return redirect()->route('admin.dashboard');
        }
    }

    public function logout()
    {
        \Auth::logout();

        return redirect()->route('login');
    }
}
