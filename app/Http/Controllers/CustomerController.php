<?php

namespace App\Http\Controllers;

use App\Models\Contact;
use App\Models\Customer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CustomerController extends Controller
{

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            \Session::put('menu','customer');
            return $next($request);
        });
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $customers = Contact::where('type','customer')->get();
        return view('admin.customer.index',compact('customers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.customer.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'name'=>'required',
            'email'=>'required|unique:contact,email',
            'phone'=>'required|numeric|min:0'
        ];

        $data = $request->all();

        $validate = Validator::make($data,$rules);
        if($validate->fails()){
            return redirect()->back()->withErrors('Validation Errors');
        }

        $contact = new Contact();
        $contact->name = $data['name'];
        $contact->email = $data['email'];
        $contact->phone = $data['phone'];
        $contact->address = $data['address'];
        $contact->type = 'customer';
        $contact->save();

        return redirect()->route('admin.customer.index')->with('success','Berhasil Menambahkan Data!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $customer = Contact::find($id);
        return view('admin.customer.edit',compact('customer'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $contact = Contact::find($id);
        $rules = [
            'name'=>'required',
            'email'=>'required|unique:contact,email,'.$id,
            'phone'=>'required|numeric|min:0'
        ];

        $data = $request->all();

        $validate = Validator::make($data,$rules);
        if($validate->fails()){
            return redirect()->back()->withErrors('Validation Errors');
        }

        $contact->name = $data['name'];
        $contact->email = $data['email'];
        $contact->phone = $data['phone'];
        $contact->address = $data['address'];
        $contact->update();

        return redirect()->route('admin.customer.index')->with('success','Berhasil Mengubah Data!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $customer = Contact::find($id);
        if(!isset($customer)){
            return redirect()->back()->withErrors('ID Not Found!');
        }
        $customer->delete();

        return redirect()->route('admin.customer.index')->with('success','Berhasil Menghapus Data!');
    }
}
