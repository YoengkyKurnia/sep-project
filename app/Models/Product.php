<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $table = 'products';

    public function product()
    {
        return $this->belongsTo(TransactionDetails::class,'id','product_id');
    }

    public function getPricingFromId($id)
    {
        $data = Product::where('id',$id)
                ->select('sell_price','purchase_price')
                ->first()->toArray();

        return $data;
    }

    public function minusQtyProductFromId($id,$minus)
    {
        $data = Product::find($id);

        if(!isset($data)){
            $output = [
                'success' => 0,
                'msg' => 'product ID tidak Ditemukan'
            ];
            return $output;
        }

        if($data->qty - $minus < 0 ){
            $output = [
                'success' => 0,
                'msg' => 'Qty tidak cukup untuk product ['.$data->name.']'
            ];
            return $output;
        }

        $data->qty = $data->qty - $minus;
        $data->save();


        $output = [
            'success' => 1,
            'msg' => 'Berhasil Menambahkan Data!'
        ];
        return $output;
    }

    public function plusQtyProductFromId($id,$plus)
    {
        $data = Product::find($id);

        if(!isset($data)){
            $output = [
                'success' => 0,
                'msg' => 'product ID tidak Ditemukan'
            ];
            return $output;
        }

        $data->qty = $data->qty + $plus;
        $data->save();


        $output = [
            'success' => 1,
            'msg' => 'Berhasil Menambahkan Data!'
        ];
        return $output;
    }
}
