<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TransactionDetails extends Model
{
    use HasFactory;
    protected $table = 'transactions_details';

    const sell = 'Sell';
    const purchase = 'Purchase';
    const purchase_return = 'Purchase Return';
    const sell_return = 'Sell Return';

    public function transactionDetail()
    {
        return $this->belongsTo(Transaction::class,'transaction_id','id');
    }

    public function product()
    {
        return $this->hasOne(Product::class,'id','product_id');
    }

    public function tipe($type)
    {
        if($type == 'sell'){
            return 'Sell';
        }else if($type == 'purchase'){
            return 'Purchase';
        }else if($type == 'purchase_return'){
            return 'Purchase Return';
        }else if($type == 'sell_return'){
            return 'Sell Return';
        }
    }

}
