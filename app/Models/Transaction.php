<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{

    const SELL = 'sell';
    const PURCHASE = 'purchase';
    const PURCHASE_RETURN = 'purchase_return';
    const SELL_RETURN = 'sell_return';

    use HasFactory;
    protected $table = 'transactions';


    public function generateInvoiceNumber($id=null)
    {
        $string = 'INV/'.date('d/m/Y').'/'.(isset($id)?$id:rand(1,100));
        return $string;
    }


    public function customer(){
        return $this->hasOne(Contact::class,'id','customer_id');
    }

    public function transactionDetails()
    {
        return $this->hasMany(TransactionDetails::class,'transaction_id','id');
    }

}
