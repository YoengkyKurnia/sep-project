<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    const CUSTOMER = 'customer';
    const SUPPLIER = 'supplier';


    use HasFactory;
    protected $table = "contact";


    public function transactions()
    {
        return $this->belongsTo(Transaction::class,'id','customer_id');
    }
}
